
#include <windows.h>
#include <tchar.h>
#include <Psapi.h>
#include <stdio.h>

void msgBox();

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)msgBox, 0,0, NULL);
		break;
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return true;
}

void msgBox() {
	DWORD pid = GetCurrentProcessId();
	char name[300] = { 0 };
	if (!GetModuleBaseNameA((HANDLE)-1, NULL, (LPSTR)&name, 300)) {
		char proc[] = "xxxx";
		memcpy(name, proc, strlen(proc));
		name[strlen(proc)] = 0;
	}

	char content[1000] = { 0 };
	sprintf_s(content, "Hello from %s (PID: %d)", name, pid);

#ifdef X_86
	char title[] = "x86 messageBox";
#elif X_64
	char title[] = "x64 messageBox";
#else
	char title[] = "messageBox";
#endif

	MessageBoxA(0, content, title, 0x40);
}