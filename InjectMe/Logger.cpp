#include "Logger.h"

Logger::Logger(){

}

Logger::Logger(int level){
	this->level = level;
}


Logger::~Logger(){
}

// 0 -> no output
// 1 -> [Error]
// 2 -> [Error] + [Info]
// 3 -> [Error] + [Info] + [Debug]
void Logger::setLevel(int level){
	this->level = level;
}

int Logger::getLevel(){
	return this->level;
}

void Logger::error(wstring log){
	DWORD last = GetLastError();
	if (level > 0){
		HANDLE hstdout = GetStdHandle(STD_OUTPUT_HANDLE);
		CONSOLE_SCREEN_BUFFER_INFO cs;
		GetConsoleScreenBufferInfo(hstdout, &cs);
		wstring finalStr = L"[Error " +e.dec2hex((LPVOID)last) + L"] " + log + L"\n";
		DWORD written = 0;
		
		SetConsoleTextAttribute(hstdout, FOREGROUND_RED);
		WriteFile(hstdout, e.w2s(finalStr).c_str(), finalStr.size(), &written, NULL);
		SetConsoleTextAttribute(hstdout,cs.wAttributes);
	}
}

void Logger::success(wstring log) {
	if (level > 0) {
		HANDLE hstdout = GetStdHandle(STD_OUTPUT_HANDLE);
		CONSOLE_SCREEN_BUFFER_INFO cs;
		GetConsoleScreenBufferInfo(hstdout, &cs);
		wstring finalStr = L"[Success] " + log + L"\n";
		DWORD written = 0;

		SetConsoleTextAttribute(hstdout, FOREGROUND_GREEN);
		WriteFile(hstdout, e.w2s(finalStr).c_str(), finalStr.size(), &written, NULL);
		SetConsoleTextAttribute(hstdout, cs.wAttributes);
	}
}

void Logger::info(wstring log){
	if (level > 1){
		wcout << L"[Info] " << log << endl;
	}
}

void Logger::debug(wstring log){
	if (level > 2){
		wcout << L"[Debug] " << log << endl;
	}
}