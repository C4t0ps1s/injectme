#pragma once

#include <windows.h>
#include <string>
#include <vector>
#include <tuple>
#include <map>
#include <TlHelp32.h>

#include "Logger.h"
#include "Engine.h"
#include "ShellCode.h"

using namespace std;

class Injector
{
public:
	Injector(Logger* log);
	~Injector();
	int InjectSC(int method, int mainSC, int pid, injector_struct is);
	int InjectRawCode(int pid, char* data, DWORD size);
	
private:
	Logger *l;
	Engine e;
	char shellCode[BUFFSC];
	int sizeSC;
	HANDLE proc;
	arg_struct *as;
	int method;
	int mainShellcode;

	int inject_createRemoteThread(LPVOID baseAddr);
	int inject_suspendedThread(LPVOID baseAddr);
	int inject_NtQueueApcThread(LPVOID baseAddr);
};

int monitoring(arg_struct *param);

