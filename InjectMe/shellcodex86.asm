format PE Console

call $ + 5	       ;call next instruction
pop eax 	       ;get EIP
add eax, 0x3f	       ;load DLL path string
push eax	       ;save position
mov eax, 0x11111111    ;@LoadLibraryA
call eax	       ;call LoadLibraryW
push 0xdeadbeef        ;padding
push eax	       ;LoadLibrary returned value
mov eax, 0x22222222    ;@GetLastError
call eax	       ;call GetLastError
push eax	       ;save GetlastError returned value
mov edi,0xaaaaaaaa     ;@return address
mov esi, esp	       ;source
mov ecx, 0xc	       ;length
rep movsb	       ;copy it
push 1		       ;returned value for current thread
mov eax, 0x3333333    ;@ExitThread
call eax	       ;call ExitThread