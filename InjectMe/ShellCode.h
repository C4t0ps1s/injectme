#pragma once

#include "windows.h"
#include <vector>
#include <tuple>
#include <map>

#include "const.h"
#include "Logger.h"
#include "Engine.h"

using namespace std;

class ShellCode
{
public:
	ShellCode(int type, LPVOID returnAddress,Logger *log);
	char* getShellCode();
	size_t getSize();
	~ShellCode();

private:
	Logger *l;
	Engine e;
	vector <tuple<LPVOID, wstring, string>> ApiRef;
	map <LPVOID, LPVOID> ApiPatch;
	char shellCode[BUFFSC];
	size_t sizeSC;
	LPVOID returnAddress;
	wstring dllPath;
	int type;

	int initAPI(LPVOID retAddr);
	int initShellCode(LPVOID retAddr);
};

