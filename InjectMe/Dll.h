#pragma once

#include <string>
#include "windows.h"
#include <iostream>

#include "Logger.h"
#include "const.h"

using namespace std;

class Dll
{
public:
	Dll(Logger* log);
	Dll(TCHAR* path,Logger *log);
	~Dll();

	int setPath(wstring path);
	wstring getPath();
	int getType();

private:
	wstring path;
	int type;
	Logger *l;

	int _getType();
};