#include "Processes.h"


Processes::Processes(Logger* log){
	this->l = log;
}


Processes::~Processes(){

}

void Processes::initList(){

	//snapshot processes
	HANDLE snap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (snap == INVALID_HANDLE_VALUE){
		l->error(L"CreateToolhelp32Snapshot");
		return;
	}
	PROCESSENTRY32 entry;
	entry.dwSize = sizeof(PROCESSENTRY32);
	Process32First(snap, &entry);

	//save tuples "PID / processName / Process type (x86 / x64)"
	do{
		wstring s(entry.szExeFile);
		BOOL is32bit = FALSE;

		//get process type
		int processType = getType(entry.th32ProcessID);

		//add tuple in list
		tuple<int, wstring, int, int> tup(entry.th32ProcessID, s, processType, isInjectable(entry.th32ProcessID));
		listProcess.push_back(tup);

	} while (Process32Next(snap, &entry));

	return;
}

void Processes::printProcesses(){

	wcout << "\n       PID    Archi Injectable  Image"  << endl;
	wcout << "---------------------------------------------------------------" << endl;

	for (auto it = listProcess.cbegin(); it != listProcess.cend(); ++it){
		wstring injectable = L"Yes";
		if (!get<3>(*it)) {
			injectable = L"No";
		}
		wcout << " \t" << e.i2w(get<0>(*it)) << " \t" << e.i2w(get<2>(*it)) << " \t" << injectable << " \t" << get<1>(*it) << endl;
	}
}

vector<int> Processes::getPid(wstring processName){
	vector<int> pidList;
	for (auto it = listProcess.cbegin(); it != listProcess.cend(); ++it){
		if (!get<1>(*it).compare(processName)){
			pidList.push_back(get<0>(*it));
		}
	}
	return pidList;
}

wstring Processes::getName(int pid){
	for (auto it = listProcess.cbegin(); it != listProcess.cend(); ++it){
		if (get<0>(*it) == pid){
			return get<1>(*it);
		}
	}
	return wstring(L"");
}

int Processes::getType(int pid){
	HANDLE hp = OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, false, pid);
	if (!hp){
		l->debug(L"Can't open Process "+e.i2w(pid));
		return 0;
	}

	TCHAR fullPath[1000] = { 0 };
	DWORD sizePath = 1000;
	if (!QueryFullProcessImageName(hp, 0,(LPTSTR)&fullPath,&sizePath)){
		l->error(L"GetProcessImageFileName");
		return ERROR;
	}
	
	l->debug(L"Path: "+wstring(fullPath));
	CloseHandle(hp);

	//disable FS redirection in system32 folder
	PVOID old;
	Wow64DisableWow64FsRedirection(&old);
	HANDLE hFile = CreateFile(fullPath, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	//enable FS redirection in system32 folder
	Wow64RevertWow64FsRedirection(old);

	if (hFile == INVALID_HANDLE_VALUE){
		l->error(L"CreateFile "+wstring(fullPath));
		return ERROR;
	}
	char buff[0x1000] = {0};

	DWORD read = 0;
	//get 0x200 first bytes 
	if (!ReadFile(hFile, buff, 0x300, &read, NULL)){
		l->error(L"ReadFile('" + wstring(fullPath) + L"')");
		return ERROR;
	}

	CloseHandle(hFile);
	unsigned int offsetNTHeader = *(int*)(buff + 0x3c);
	if (offsetNTHeader > 0x300){
		l->error(L"Not a valid PE file");
		return ERROR;
	}

	DWORD sign = *(DWORD*)(buff + offsetNTHeader);
	if (sign != 0x4550){
		l->error(L"Not a valid PE file - can't find PE signature");
		return ERROR;
	}
	WORD magic = *(WORD*)(buff + offsetNTHeader + 0x18);

	if (magic == 0x010b){
		return X86;
	}
	else if (magic == 0x020b){
		return X64;
	}
	else{
		l->error(L"Unknown PE type");
		return ERROR;
	}
}

int Processes::isInjectable(int pid) {
	HANDLE hp = OpenProcess(PROCESS_ALL_ACCESS, false, pid);
	if (!hp) {
		return 0;
	}
	else {
		CloseHandle(hp);
		return 1;
	}
}

HMODULE Processes::isDllInProcess(int pid, wstring dll) {
	HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, 0, pid);
	HMODULE moduleList[1024];
	DWORD size = 0;
	EnumProcessModules(hProcess, moduleList, sizeof(moduleList), &size);

	for (int i = 0; i < (size / sizeof(HMODULE)); i++){
		TCHAR modName[MAX_PATH] = { 0 };
		GetModuleFileNameEx(hProcess, moduleList[i], modName, 300);

		//keep just module name (remove full path)
		TCHAR* lastSlash = wcsrchr(modName, '\\');
		wstring ws_modName(lastSlash+1);

		//to lower
		ws_modName = e.toLower(ws_modName);
		dll = e.toLower(dll);

		if (!dll.compare(ws_modName)) {
			return moduleList[i];
		}
	}

	return 0;
}
