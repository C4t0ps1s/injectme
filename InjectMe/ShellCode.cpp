#include "ShellCode.h"


ShellCode::ShellCode(int type, LPVOID returnAddress, Logger *log){
	this->returnAddress = returnAddress;
	this->dllPath = dllPath;
	this->l = log;
	this->type = type;
	sizeSC = 0;

	//prepare shellcode for targeted process (x86 or x64)
	if (!initShellCode(returnAddress)){
		l->error(L"Error during shellcode initialisation");
		return;
	}	
}


ShellCode::~ShellCode(){
	//free(fullSC);
}

int ShellCode::initAPI(LPVOID retAddr){

	//clean vector
	ApiRef.clear();
	ApiPatch.clear();

	if ((type & SC_LOADDLL) == SC_LOADDLL) {
#if X_86
		ApiRef.push_back(tuple<LPVOID, wstring, string>((LPVOID)0x11111111, L"kernel32.dll", "LoadLibraryW"));
		ApiRef.push_back(tuple<LPVOID, wstring, string>((LPVOID)0x22222222, L"kernel32.dll", "GetLastError"));		
		ApiPatch[(LPVOID)0xaaaaaaaa] = retAddr;
#else
		ApiRef.push_back(tuple<LPVOID, wstring, string>((LPVOID)0x1111111111111111, L"kernel32.dll", "LoadLibraryW"));
		ApiRef.push_back(tuple<LPVOID, wstring, string>((LPVOID)0x2222222222222222, L"kernel32.dll", "GetLastError"));		
		ApiPatch[(LPVOID)0xaaaaaaaaaaaaaaaa] = retAddr;
#endif
	}

	if ((type & SC_UNLOADDLL) == SC_UNLOADDLL) {
#if X_86
		ApiRef.push_back(tuple<LPVOID, wstring, string>((LPVOID)0x11111111, L"kernel32.dll", "FreeLibrary"));
		ApiRef.push_back(tuple<LPVOID, wstring, string>((LPVOID)0x22222222, L"kernel32.dll", "GetLastError"));
		ApiPatch[(LPVOID)0xaaaaaaaa] = retAddr;
#else
		ApiRef.push_back(tuple<LPVOID, wstring, string>((LPVOID)0x1111111111111111, L"kernel32.dll", "FreeLibrary"));
		ApiRef.push_back(tuple<LPVOID, wstring, string>((LPVOID)0x2222222222222222, L"kernel32.dll", "GetLastError"));
		ApiPatch[(LPVOID)0xaaaaaaaaaaaaaaaa] = retAddr;
#endif
	}

	if ((type & SC_MSGBOX) == SC_MSGBOX) {
#ifdef X_86
		ApiRef.push_back(tuple<LPVOID, wstring, string>((LPVOID)0x11111111, L"kernel32.dll", "LoadLibraryA"));
		ApiRef.push_back(tuple<LPVOID, wstring, string>((LPVOID)0x22222222, L"kernel32.dll", "GetCurrentProcessId"));
		ApiRef.push_back(tuple<LPVOID, wstring, string>((LPVOID)0x33333333, L"psapi.dll", "GetModuleBaseNameA"));
		ApiRef.push_back(tuple<LPVOID, wstring, string>((LPVOID)0x44444444, L"ntdll.dll", "sprintf"));
		ApiRef.push_back(tuple<LPVOID, wstring, string>((LPVOID)0x55555555, L"User32.dll", "MessageBoxA"));
		ApiPatch[(LPVOID)0xaaaaaaaa] = retAddr;
#else
		ApiRef.push_back(tuple<LPVOID, wstring, string>((LPVOID)0x1111111111111111, L"kernel32.dll", "LoadLibraryA"));
		ApiRef.push_back(tuple<LPVOID, wstring, string>((LPVOID)0x2222222222222222, L"kernel32.dll", "GetCurrentProcessId"));
		ApiRef.push_back(tuple<LPVOID, wstring, string>((LPVOID)0x3333333333333333, L"psapi.dll", "GetModuleBaseNameA"));
		ApiRef.push_back(tuple<LPVOID, wstring, string>((LPVOID)0x4444444444444444, L"ntdll.dll", "sprintf"));
		ApiRef.push_back(tuple<LPVOID, wstring, string>((LPVOID)0x5555555555555555, L"User32.dll", "MessageBoxA"));
		ApiPatch[(LPVOID)0xaaaaaaaaaaaaaaaa] = retAddr;
#endif
	}

	if ((type & SC_EXITTHREAD) == SC_EXITTHREAD) {
#if X_86
		ApiRef.push_back(tuple<LPVOID, wstring, string>((LPVOID)0x66666666, L"kernel32.dll", "ExitThread"));
#else
		ApiRef.push_back(tuple<LPVOID, wstring, string>((LPVOID)0x6666666666666666, L"kernel32.dll", "ExitThread"));
#endif
	}

	if ((type & SC_SLEEP) == SC_SLEEP){
#if X_86
		ApiRef.push_back(tuple<LPVOID, wstring, string>((LPVOID)0x66666666, L"kernel32.dll", "WaitForSingleObject"));
#else
		ApiRef.push_back(tuple<LPVOID, wstring, string>((LPVOID)0x6666666666666666, L"kernel32.dll", "WaitForSingleObject"));
#endif
	}


	

	//resolve API address for shellcode
	for (auto it = ApiRef.cbegin(); it != ApiRef.cend(); ++it){
		HMODULE hModule = GetModuleHandle(get<1>(*it).c_str());
		if (hModule == 0) {
			if (GetLastError() == ERROR_MOD_NOT_FOUND) {
				hModule = LoadLibrary(get<1>(*it).c_str());
				if (hModule == 0) {
					l->error(L"Loading module "+ get<1>(*it));
					return 0;
				}
			}
		}
		LPVOID addrAPI = GetProcAddress(hModule, get<2>(*it).c_str());
		if (addrAPI){
			ApiPatch[get<0>(*it)] = addrAPI;
			l->debug(get<1>(*it) + L"!" + e.s2w(get<2>(*it)) + L": " + e.dec2hex(addrAPI));
		}
		else{
			l->error(L"GetProcAddress(" + e.s2w(get<2>(*it)) + L")");
			return 0;
		}
	}
	return 1;
}

int ShellCode::initShellCode(LPVOID retAddr){

	//get tuple < match need to be changed / API addresses > 
	if (initAPI(retAddr) == 0){
		return 0;
	}

	char sc[BUFFSC] = { 0 };
	char* currentPosition = sc;
	if ((type & SC_LOADDLL) == SC_LOADDLL) {

#if X_86
		int sizeSC_tmp = 44;
		char sc_tmp[] =
			"\xb8\xaa\xaa\xaa\xaa"	//mov edi,@Shared memory
			"\x83\xc0\x10"			//add eax,0x10
			"\x50"					//push eax
			"\xb8\x11\x11\x11\x11"	//mov eax,@LoadLibraryW
			"\xff\xd0"				//call eax
			"\x68\xef\xbe\xad\xde"	//push 0xdeadbeef
			"\x50"					//push eax
			"\xb8\x22\x22\x22\x22"	//mov eax,@GetLastError
			"\xff\xd0"				//call eax
			"\x50"					//push eax
			"\xbf\xaa\xaa\xaa\xaa"	//mov edi,@Shared memory
			"\x89\xe6"				//mov esi,esp
			"\xb9\x0c\x00\x00\x00"	//mov ecx,0x0c
			"\xf3\xa4";				//rep movsb

		memcpy(currentPosition, sc_tmp, sizeSC_tmp);
		sizeSC += sizeSC_tmp;
		currentPosition += sizeSC_tmp;
#else
		int sizeSC_tmp = 76;
		char sc_tmp[] =
			"\x48\xb9\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"	//mov rcx,@Shared memory
			"\x48\x83\xc1\x20"							//add rcx,0x20			
			"\x48\xb8\x11\x11\x11\x11\x11\x11\x11\x11"	//mov rax,@LoadLibraryW
			"\x6a\x00"									//push 0x00
			"\xff\xd0"									//call rax
			"\x59"										//pop rcx
			"\x48\xbb\xef\xbe\xad\xde\x00\x00\x00\x00"	//mov rbx,0xdeadbeef
			"\x53"										//push rbx
			"\x50"										//push rax
			"\x48\xb8\x22\x22\x22\x22\x22\x22\x22\x22"	//mov rax,@GetLastError
			"\xff\xd0"									//call rax
			"\x50"										//push rax
			"\x48\xbf\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"	//mov rdi,@Shared memory
			"\x48\x89\xe6"								//mov rsi,rsp
			"\x48\xc7\xc1\x18\x00\x00\x00"				//mov rcx,0x18
			"\xf3\xa4";									//rep movsb

		memcpy(currentPosition, sc_tmp, sizeSC_tmp);
		sizeSC += sizeSC_tmp;
		currentPosition += sizeSC_tmp;
#endif

	}

	else if ((type & SC_UNLOADDLL) == SC_UNLOADDLL) {

#if X_86
		int sizeSC_tmp = 45;
		char sc_tmp[] =
			"\xb8\xaa\xaa\xaa\xaa"	//mov edi,@Shared memory
			"\x83\xc0\x10"			//add eax,0x10
			"\xff\x30"				//push [eax]
			"\xb8\x11\x11\x11\x11"	//mov eax,@FreeLibrary
			"\xff\xd0"				//call eax
			"\x68\xef\xbe\xad\xde"	//push 0xdeadbeef
			"\x50"					//push eax
			"\xb8\x22\x22\x22\x22"	//mov eax,@GetLastError
			"\xff\xd0"				//call eax
			"\x50"					//push eax
			"\xbf\xaa\xaa\xaa\xaa"	//mov edi,@Shared memory
			"\x89\xe6"				//mov esi,esp
			"\xb9\x0c\x00\x00\x00"	//mov ecx,0x0c
			"\xf3\xa4";				//rep movsb

		memcpy(currentPosition, sc_tmp, sizeSC_tmp);
		sizeSC += sizeSC_tmp;
		currentPosition += sizeSC_tmp;

#else
		int sizeSC_tmp = 76;
		char sc_tmp[] =
			"\x48\xb9\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa" //mov rcx,@Shared memory
			"\x48\x83\xc1\x20"                         //add rcx,0x20
			"\x48\x8b\x09"                             //mov rcx,[rcx]
			"\x48\xb8\x11\x11\x11\x11\x11\x11\x11\x11" //mov rax, @FreeLibrary
			"\xff\xd0"                                 //call rax
			"\x48\xbb\xef\xbe\xad\xde\x00\x00\x00\x00" //mov rbx, 0xdeadbeef
			"\x53"                                     //push rbx
			"\x50"                                     //push rax
			"\x48\xb8\x22\x22\x22\x22\x22\x22\x22\x22" //mov rax,@GetLastError
			"\xff\xd0"                                 //call rax
			"\x50"                                     //push rax
			"\x48\xbf\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa" //mov rdi,@Shared memory
			"\x48\x89\xe6"                             //mov rsi,rsp
			"\x48\xc7\xc1\x18\x00\x00\x00"             //mov rcx, 0x18
			"\xf3\xa4";                                //rep movsb

		memcpy(currentPosition, sc_tmp, sizeSC_tmp);
		sizeSC += sizeSC_tmp;
		currentPosition += sizeSC_tmp;
#endif

	}

	else if ((type & SC_MSGBOX) == SC_MSGBOX) {
#ifdef X_86
		int sizeSC_tmp = 190;
		char sc_tmp[] =
			//Load necessary DLL : User32.dll & psapi.dll
			"\xB8\x11\x11\x11\x11"  //mov eax,@LoadLibraryA
			"\x50"			        //push eax
			"\x68\x6C\x6C\x00\x00"  //push "User32.dll"
			"\x68\x33\x32\x2E\x64"
			"\x68\x55\x73\x65\x72"
			"\x54"                  //push esp
			"\xFF\xD0"              //call eax
			"\x83\xC4\x0C"          //add esp,0x0c
			"\x58"                  //pop eax
			"\x6A\x6C"              //push 0x6c
			"\x68\x69\x2E\x64\x6C"  //push "psapi.dll"
			"\x68\x70\x73\x61\x70"
			"\x54"                  //push esp
			"\xFF\xD0"              //call eax

			//get PID of current process
			"\xB8\x22\x22\x22\x22"  //mov eax,@GetCurrentProcessID
			"\xFF\xD0"              //call eax
			"\x50"                  //push eax

			//alloc 0x20 bytes and fill it with 0x00
			"\x83\xEC\x20"          //sub esp,0x20
			"\x89\xE7"              //mov edi,esp
			"\xB9\x20\x00\x00\x00"  //mov ecx,0x20
			"\xB0\x00"              //mov al,0x00
			"\xF3\xAA"              //rep stosb

			//save name of current process in previous buffer
			"\x89\xE0"              //mov eax,esp
			"\x6A\x20"              //push 0x20
			"\x50"                  //push eax
			"\x6A\x00"              //push 0x00
			"\x6A\xFF"              //push 0xffffffff
			"\xB8\x33\x33\x33\x33"  //mov eax,@GeyModuleBaseNameA
			"\xFF\xD0"              //call eax

			//alloc 0x50 bytes and fill it with 0x00
			"\x89\xE3"              //mov ebx,esp
			"\x83\xEC\x50"          //sub esp,0x50
			"\x89\xE7"              //mov edi,esp
			"\xB9\x50\x00\x00\x00"  //mov ecx,0x50
			"\xB0\x00"              //mov al,0x00
			"\xF3\xAA"              //rep stosb

			//save buffer
			"\x89\xE0"              //mov eax,esp

			//get PID
			"\x8B\x54\x24\x70"      //mov edx,[esp+0x70]

			//prepare messagebox content
			"\x68\x25\x64\x29\x00"  //push "Hello from %s (PID: %d)"
			"\x68\x49\x44\x3A\x20"
			"\x68\x73\x20\x28\x50"
			"\x68\x6F\x6D\x20\x25"
			"\x68\x6F\x20\x66\x72"
			"\x68\x48\x65\x6C\x6C"
			"\x89\xE1"              //mov ecx,esp
			"\x52"                  //push edx
			"\x53"                  //push ebx
			"\x51"                  //push ecx
			"\x50"                  //push eax
			"\xB8\x44\x44\x44\x44"  //mov eax,@sprintf
			"\xFF\xD0"              //call eax

			//launch messagebox
			"\xB8\x55\x55\x55\x55"  //mov eax,@MessageBoxA
			"\x5B"                  //pop ebx
			"\x68\x5C\x6F\x2F\x00"  //push "\o/"
			"\x89\xE1"              //mov ecx,esp
			"\x6A\x40"              //push 0x40
			"\x51"                  //push ecx
			"\x53"                  //push ebx
			"\x6A\x00"              //push 0x00
			"\xFF\xD0"              //call eax

		//send end of shellcode in shared memory
			"\x68\xef\xbe\xad\xde"  //push 0xdeadbeef
			"\x6a\x01"              //push 0x01
			"\x6a\x00"              //push 0x00
			"\xbf\xaa\xaa\xaa\xaa"  //mov edi,@Shared memory
			"\x89\xe6"              //mov esi,esp
			"\xb9\x0c\x00\x00\x00"  //mov ecx,0x0c
			"\xf3\xa4";             //rep movsb

		memcpy(currentPosition, sc_tmp, sizeSC_tmp);
		sizeSC += sizeSC_tmp;
		currentPosition += sizeSC_tmp;
#else
		int sizeSC_tmp = 308;
		char sc_tmp[] =
			"\x48\xB8\x11\x11\x11\x11\x11\x11\x11\x11" //mov rax,@LoadLibraryA
			"\x50"                                     //push rax
			"\x48\xC7\xC3\x6C\x6C\x00\x00"             //push "user32.dll"
			"\x53"
			"\x48\xBB\x75\x73\x65\x72\x33\x32\x2E\x64"
			"\x53"
			"\x48\x89\xE1"                             //mov rcx,rsp
			"\x6A\x00"                                 //push 0x00
			"\x6A\x00"                                 //push 0x00
			"\xFF\xD0"                                 //call rax
			"\x48\x83\xC4\x20"                         //add rsp,0x20
			"\x5B"                                     //pop rbx
			"\x48\xC7\xC0\x6C\x00\x00\x00"             //push "psapi.dll"
			"\x50"
			"\x48\xB8\x70\x73\x61\x70\x69\x2E\x64\x6C"
			"\x50"
			"\x48\x89\xE1"                             //mov rcx,rsp
			"\x6A\x00"                                 //push 0x00
			"\x6A\x00"                                 //push 0x00
			"\xFF\xD3"                                 //call rbx
			"\x48\xB8\x22\x22\x22\x22\x22\x22\x22\x22" //mov rax,@GetCurrentProcessId
			"\xFF\xD0"                                 //call rax
			"\x50"                                     //push rax
			"\x48\x83\xEC\x20"                         //sub rsp,0x20
			"\x48\x89\xE7"                             //mov rdi,rsp
			"\x48\xC7\xC1\x20\x00\x00\x00"             //mov rcx,0x20
			"\xB0\x00"                                 //mov al,0x00
			"\xF3\xAA"                                 //rep stosb
			"\x48\x89\xE0"                             //mov rax,rsp
			"\x49\xC7\xC1\x20\x00\x00\x00"             //mov r9,0x20
			"\x49\x89\xC0"                             //mov r8,rax
			"\x48\x31\xD2"                             //xor rdx,rdx
			"\x48\x31\xC9"                             //xor rcx,rcx
			"\x48\xFF\xC9"                             //dec rcx
			"\x48\xB8\x33\x33\x33\x33\x33\x33\x33\x33" //mov rax,@GetModuleBaseNameA
			"\xFF\xD0"                                 //call rax
			"\x49\x89\xE1"                             //mov r9,rsp

			//get PID
			"\x67\x4C\x8B\x44\x24\x20"                 //mov r8,[rsp+0x20]
			"\x6a\x00"								   //push 0x00
			"\x48\xbb\x64\x20\x28\x25\x73\x29\x00\x00" //push "Hello from PID %d (%s)"
			"\x53"
			"\x48\xbb\x6f\x6d\x20\x50\x49\x44\x20\x25"
			"\x53"
			"\x48\xbb\x48\x65\x6c\x6c\x6f\x20\x66\x72"
			"\x53"
			"\x48\x89\xe2"                             //mov rdx,rsp
			"\x48\x83\xEC\x50"                         //sub rsp,0x50
			"\x48\x89\xE7"                             //mov rdi,rsp
			"\x48\xC7\xC1\x50\x00\x00\x00"             //mov rcx,0x50
			"\xB0\x00"                                 //mov al,0x00
			"\xF3\xAA"                                 //rep stosb
			"\x48\x89\xE1"                             //mov rcx,rsp
			"\x48\xB8\x44\x44\x44\x44\x44\x44\x44\x44" //mov rax,@sprintf
			"\xFF\xD0"                                 //call rax
			"\x48\x89\xE2"                             //mov rdx,rsp

			"\x6a\x00"								   //push 0x00
			"\x48\xbb\x61\x67\x65\x42\x6f\x78\x00\x00" //push "x64 messageBox"
			"\x53"
			"\x48\xbb\x78\x36\x34\x20\x6d\x65\x73\x73"
			"\x53"
			"\x49\x89\xe0"                             //mov r8,rsp

			"\x49\xC7\xC1\x40\x00\x00\x00"             //mov r9,0x40
			"\x48\x31\xC9"                             //xor rcx,rcx
			"\x48\xB8\x55\x55\x55\x55\x55\x55\x55\x55" //mov rax,@MessageBoxA
			"\x6a\x00"								   //push 0x00
			"\xFF\xD0"                                 //call rax

		//send end of shellcode in shared memory
		    "\x48\xbb\xef\xbe\xad\xde\x00\x00\x00\x00"  //push 0xdeadbeef
			"\x53"                                      //push rbx
			"\x6a\x01"                                  //push 0x01
			"\x6a\x00"                                  //push 0x00
			"\x48\xbf\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"  //mov edi,@Shared memory
			"\x48\x89\xe6"                              //mov rsi,rsp
			"\x48\xc7\xc1\x1c\x00\x00\x00"              //mov rcx,0x1c
			"\xf3\xa4";                                 //rep movsb


		memcpy(currentPosition, sc_tmp, sizeSC_tmp);
		sizeSC += sizeSC_tmp;
		currentPosition += sizeSC_tmp;
#endif

	}

	if ((type & SC_EXITTHREAD) == SC_EXITTHREAD) {
#ifdef X_86
		int sizeSC_tmp = 9;
		char sc_tmp[] =
			"\x6a\x01"				//push 0x01
			"\xb8\x66\x66\x66\x66"	//mov eax,@ExitThread
			"\xff\xd0";				//call eax
#else
		int sizeSC_tmp = 14;
		char sc_tmp[] =
			"\x6a\x01"									//push 0x1
			"\x48\xb8\x66\x66\x66\x66\x66\x66\x66\x66"	//mov rax,@ExitThread
			"\xff\xd0";									//call rax
#endif

		memcpy(currentPosition, sc_tmp, sizeSC_tmp);
		sizeSC += sizeSC_tmp;
		currentPosition += sizeSC_tmp;

	}

	if ((type & SC_SLEEP) == SC_SLEEP) {
#ifdef X_86
		int sizeSC_tmp = 23;
		char sc_tmp[] =
			"\xe8\x00\x00\x00\x00"   //call $+5
			"\x31\xdb"               //xor ebx,ebx
			"\x4b"                   //dec ebx
			"\x6a\x50"               //push 0x50
			"\x53"                   //push ebx
			"\xb8\x66\x66\x66\x66"   //mov eax, @WaitForSingleObject
			"\xff\xd0"               //call eax
			"\x83\x2c\x24\x05"       //sub DWORD [esp],5
			"\xc3";                  //ret
#else
		int sizeSC_tmp = 36;
		char sc_tmp[] =
			"\xe8\x00\x00\x00\x00"                     //call $+5
			"\x48\x31\xc9"                             //xor rcx,rcx
			"\x48\xff\xc9"                             //dec rcx
			"\x48\xc7\xc2\x50\x00\x00\x00"             //mov rdx,0x50
			"\x48\xb8\x66\x66\x66\x66\x66\x66\x66\x66" //mov rax,@WaitForSingleObject
			"\xff\xd0"                                 //call rax
			"\x48\x83\x2c\x24\x05"                     //sub [rsp],0x05
			"\xc3";                                    //ret
#endif

		memcpy(currentPosition, sc_tmp, sizeSC_tmp);
		sizeSC += sizeSC_tmp;
		currentPosition += sizeSC_tmp;

	}

	int apiPatched = 0;
	//patch shellcode with good API addresses
	for (unsigned int i = 0; i < sizeSC; i++){

		//foreach raw API address 
		for (auto itRef = ApiRef.cbegin(); itRef != ApiRef.cend(); ++itRef){

			//foreach resolved API address
			for (auto itpatch = ApiPatch.cbegin(); itpatch != ApiPatch.cend(); ++itpatch){

				if (*(LPVOID*)(sc + i) == (LPVOID)get<0>(*itpatch)){
					*(LPVOID*)(sc + i) = (LPVOID)get<1>(*itpatch);
					apiPatched++;
				}
			}
		}
	}

	if (apiPatched < ApiPatch.size()){
		l->error(L"Fail during API patching");
		return 0;
	}
	if (!memcpy(this->shellCode, sc, sizeSC)){
		l->error(L"memcpy");
		return 0;
	}
	return 1;
}

char* ShellCode::getShellCode(){
	return shellCode;
}

size_t ShellCode::getSize(){
	return sizeSC;
}