
#pragma once

#include <string>
#include <windows.h>

using namespace std;

#define X64 64
#define X86 32
#define UNKNOWN 2

#define BUFFSC 500

#define SC_LOADDLL 1
#define SC_UNLOADDLL 2
#define SC_MSGBOX 4
#define SC_SLEEP 8
#define SC_EXITTHREAD 16


#define INJECT_METHOD_DEFAULT 0
#define INJECT_METHOD_CREATEREMOTETHREAD 1
#define INJECT_METHOD_SUSPENDEDTHREAD 2
#define INJECT_METHOD_NTQUEUEAPCTHREAD 3

typedef struct {
	int pid;
	LPVOID address;
	int logLevel;
	int mainShellcode;
} arg_struct;

typedef struct {

	HMODULE dllModule = NULL;;
	wstring dllPath = L"";
} injector_struct;

class Const
{
public:
	Const();
	~Const();

	const wstring s_method[4] = { L"Default", L"CreateRemoteThread", L"SuspendThread", L"NtQueueApcThread" };

};
