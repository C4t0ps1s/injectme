#include "Usage.h"



Usage::Usage(){
}


Usage::~Usage(){
}

int Usage::print(wstring exeName) {
#ifdef X_86
	wcout << L"\n--- Injector 32 bits ---\n" << endl;
#else
	wcout << L"\n--- Injector 64 bits ---\n" << endl;
#endif
	wcout << L"Usage: "+exeName+L" <Source> <Destination> [Options]" << endl;
	wcout << endl;
	wcout << L"SOURCE:" << endl;
	wcout << L"    -d <Dllpath>     injected Dll path" << endl;
	wcout << L"    -u <Dllname>     Unload targeted DLL in remote process" << endl;
	wcout << L"    -r <RawCodepath> injected raw shellcode path" << endl;
	wcout << L"    -rs              raw shellcode sample (messageBox)" << endl;
	wcout << endl;
	wcout << L"DESTINATION:" << endl;
	wcout << L"    -p <PID or name> targeted process" << endl;
	wcout << endl;
	wcout << L"OPTIONS:" << endl;
	wcout << L"    -v <level>       verbosity level: [0..3]" << endl;
	wcout << L"    -m <method>      injection method: [1,2]" << endl;
	wcout << L"        1 - CreateRemoteThread (default)" << endl;
	wcout << L"        2 - SuspendThread               " << endl;
	//wcout << L"        3 - NtQueueApcThread   (ToDo...)" << endl;
	wcout << endl;
	wcout << L"MISC:" << endl;
	wcout << L"    -l               show processes and exit" << endl;
#ifdef X_86
	wcout << L"    -D               Drop x64 version of injector in current folder and exit" << endl;
#endif
	wcout << L"    -h | ?           Print this help and exit" << endl;
	return 1;
}