#pragma once

#include <iostream>
#include <string>
#include <tchar.h>

using namespace std;

class Usage
{
public:
	Usage();
	~Usage();

	int print(wstring exeName);
};

