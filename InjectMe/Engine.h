#pragma once

#include "windows.h"
#include <string>
#include <math.h>
#include <sstream>

#include "const.h"

using namespace std;

class Engine{
public:
	Engine();
	~Engine();
	DWORD64 w2i(wstring input);
	wstring dec2hex(wstring decString);
	wstring dec2hex(LPVOID decValue);
	wstring s2w(string str);
	string w2s(wstring str);
	wstring i2w(int input);
	wstring i2w(DWORD64 input);
	wstring toLower(wstring input);
};

