#include "Injector.h"

Injector::Injector(Logger* log){
	this->l = log;
	sizeSC = 0;
}


Injector::~Injector(){
}

int Injector::InjectSC(int method, int mainSC,int pid, injector_struct is){

	this->method = method;
	this->mainShellcode = mainSC;

	//get Handle for target process
	proc = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
	if (!proc) {
		l->error(L"OpenProcess(" + to_wstring(pid) + L")");
		return 0;
	}

	LPVOID addrReturn = NULL;
	LPVOID addrDll = NULL;

	//Allocate shared memory zone
	if (!is.dllPath.empty()) {
		addrReturn = VirtualAllocEx(proc, NULL, sizeof(LPVOID) * 4 + (is.dllPath.size() + 1) * 2, MEM_COMMIT, PAGE_READWRITE);
	}
	else if (is.dllModule != NULL){
		addrReturn = VirtualAllocEx(proc, NULL, sizeof(LPVOID) * 5, MEM_COMMIT, PAGE_READWRITE);
	}
	else {
		addrReturn = VirtualAllocEx(proc, NULL, sizeof(LPVOID) * 4, MEM_COMMIT, PAGE_READWRITE);
	}
	
	if (!addrReturn) {
		l->error(L"VirtualAllocEx return address");
		return 0;
	}
	else {
		l->info(L"Shared address: " + e.dec2hex(addrReturn));
	}

	//compute shared address if necessary
	addrDll = (LPVOID)((DWORD)addrReturn + (sizeof(LPVOID) * 4));

	int retwm = 1;
	//copy dllpath in remote process
	if (!is.dllPath.empty()) {
		retwm = WriteProcessMemory(proc, addrDll, is.dllPath.c_str(), (is.dllPath.size() + 1) * 2, NULL);
	}

	//or copy his module handle
	else if (is.dllModule != NULL) {
		retwm = WriteProcessMemory(proc, addrDll, (char*)&is.dllModule, sizeof(HMODULE), NULL);
	}
	
	if (!retwm) {
		l->error(L"WriteProcessMemory");
		return 0;
	}
	
	//generate adapted shellcode
	ShellCode* sc;
	if (method == INJECT_METHOD_CREATEREMOTETHREAD) {
		sc = new ShellCode(mainSC | SC_EXITTHREAD, addrReturn, l);
	}
	else if (method == INJECT_METHOD_SUSPENDEDTHREAD) {
		sc = new ShellCode(mainSC | SC_SLEEP, addrReturn, l);
	}
	else if (method == INJECT_METHOD_NTQUEUEAPCTHREAD) {
		l->error(L"Not yet implemented");
		return 0;
	}
	else {
		l->error(L"Bad injection method, exiting...");
		return 0;
	}

	//Allocate memory zone to put shellcode
	LPVOID baseAddr = VirtualAllocEx(proc, NULL, sc->getSize(), MEM_COMMIT, PAGE_READWRITE);
	if (!baseAddr) {
		l->error(L"VirtualAllocEx shellcode address");
		return 0;
	}
	else {
		l->info(L"Injected code address: " + e.dec2hex(baseAddr));
	}

	//Write shellcode in remote process
	retwm = WriteProcessMemory(proc, baseAddr, sc->getShellCode(), sc->getSize(), NULL);
	if (!retwm) {
		l->error(L"WriteProcessMemory");
		return 0;
	}

	//set shellcode address as executable
	DWORD oldProtect = 0;
	if (!VirtualProtectEx(proc, baseAddr, sc->getSize(), PAGE_EXECUTE_READ, &oldProtect)) {
		l->error(L"VirtualProtectEx");
		return 0;
	}

	//save shared memory informations
	as = (arg_struct *)malloc(sizeof(arg_struct));
	as->address = addrReturn;
	as->pid = pid;
	as->logLevel = l->getLevel();
	as->mainShellcode = mainSC;

	//inject shellcode with adapted method
	if (method == INJECT_METHOD_CREATEREMOTETHREAD) {
		return inject_createRemoteThread(baseAddr);
	}
	else if (method == INJECT_METHOD_SUSPENDEDTHREAD) {
		return inject_suspendedThread(baseAddr);
	}
	else if (method == INJECT_METHOD_NTQUEUEAPCTHREAD) {
		l->error(L"Not yet implemented");
		return 0;
	}
	else {
		l->error(L"Bad injection method, exiting...");
		return 0;
	}

}

int Injector::InjectRawCode(int pid, char* data, DWORD size) {

	//get Handle for target process
	HANDLE proc = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
	if (!proc) {
		l->error(L"OpenProcess(" + to_wstring(pid) + L")");
		return 0;
	}

	//Allocate memory zone for raw code
	LPVOID baseAddr = VirtualAllocEx(proc, NULL,size, MEM_COMMIT, PAGE_READWRITE);
	if (!baseAddr) {
		l->error(L"VirtualAllocEx return address");
		return 0;
	}
	else {
		l->info(L"Injected code address: " + e.dec2hex(baseAddr));
	}

	//Write Rawcode in remote process
	int n = WriteProcessMemory(proc, baseAddr, data, size, NULL);
	if (!n) {
		l->error(L"WriteProcessMemory");
		return 0;
	}

	//set Raw code as executable
	DWORD oldProtect = 0;
	if (!VirtualProtectEx(proc, baseAddr, size, PAGE_EXECUTE_READ, &oldProtect)) {
		l->error(L"VirtualProtectEx");
		return 0;
	}

	//Launch remote thread
	HANDLE hThread = CreateRemoteThread(proc, NULL, 0, (LPTHREAD_START_ROUTINE)baseAddr, NULL, NULL, NULL);
	if (!hThread) {
		l->error(L"CreateRemoteThread");
		return 0;
	}
	return 1;
}

int monitoring(arg_struct *param){
	
	Engine e;
	arg_struct *as = param;
	Logger log(as->logLevel);

	char buff[sizeof(LPVOID)*3] = { 0 };
	SIZE_T read = 0;
	bool found = false;

	LPVOID retGetLastError = 0;
	LPVOID retLoadLibrary = 0;

	HANDLE hProc = OpenProcess(PROCESS_ALL_ACCESS, FALSE, as->pid);
	if (!hProc){
		log.error(L"OpenProcess");
		return 0;
	}

	while (!found){
		int n = ReadProcessMemory(hProc, (LPVOID)as->address, buff, sizeof(LPVOID) * 3, &read);
		if (!n){
			log.error(L"ReadProcessMemory");
			return 0;
		}

		LPVOID padding = *(LPVOID*)(buff + sizeof(LPVOID)*2);

		if (padding == (LPVOID)0xdeadbeef){
			retGetLastError = *(LPVOID*)(buff);
			retLoadLibrary = *(LPVOID*)(buff + sizeof(LPVOID));
			found = true;
		}
		WaitForSingleObject(GetCurrentThread(), 20);
	}

	log.debug(L"retGetLastError: " + e.dec2hex(retGetLastError));
	log.debug(L"retLoadLibrary: " + e.dec2hex(retLoadLibrary));

	//Success case
	if (retLoadLibrary){
		if ((as->mainShellcode & SC_LOADDLL) == SC_LOADDLL) {
			log.success(L"[" + e.dec2hex(to_wstring(as->pid)) + L"] Success :) Dll injected into address 0x" + e.dec2hex(retLoadLibrary));
		}
		if ((as->mainShellcode & SC_UNLOADDLL) == SC_UNLOADDLL) {
			log.success(L"[" + e.dec2hex(to_wstring(as->pid)) + L"] Success :) Dll unloaded");
		}
		if ((as->mainShellcode & SC_MSGBOX) == SC_MSGBOX) {
			log.success(L"End of Shellcode reached");
		}

		
	}

	//Error case
	else{
		log.error(L"PID " + e.dec2hex(to_wstring(as->pid)) + L": remote call error = " + e.dec2hex(retGetLastError));
		return 0;
	}
	CloseHandle(hProc);
	return 1;
}

int Injector::inject_createRemoteThread(LPVOID baseAddr) {

	//Launch remote thread
	HANDLE hThread = CreateRemoteThread(proc, NULL, 0, (LPTHREAD_START_ROUTINE)baseAddr, NULL, NULL, NULL);
	if (!hThread) {
		l->error(L"CreateRemoteThread");
		return 0;
	}

	//launch monitor thread
	l->info(L"Starting monitoring...");
	
	HANDLE controlThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)monitoring, as, 0, NULL);

	int eventWait = WaitForSingleObject(controlThread, 500000);
	if (eventWait == WAIT_TIMEOUT) {
		l->error(L"Timeout reached, DLL probably not injected :/");
		return 0;
	}
	return 1;
}

int Injector::inject_suspendedThread(LPVOID baseAddr) {

	//do whole snapshot 
	HANDLE hsnapThread = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD,0);
	THREADENTRY32 te;
	te.dwSize = sizeof(te);
	BOOL res = Thread32First(hsnapThread, &te);
	HANDLE hThread = NULL;

	//looking for thread to hijack
	do {
		if (te.th32OwnerProcessID == as->pid) {
			hThread = OpenThread(THREAD_ALL_ACCESS, FALSE, te.th32ThreadID);
			if (hThread) {
				break;
			}
		}	
	} while (Thread32Next(hsnapThread, &te));

	if (!hThread) {
		l->error(L"Can't Open thread");
		return 0;
	}
	//suspend thread and modify EIP
	if (SuspendThread(hThread) == -1) {
		l->error(L"SuspendThread");
		return 0;
	}
	CONTEXT context;
	context.ContextFlags = CONTEXT_FULL;

	if (!GetThreadContext(hThread, &context)) {
		l->error(L"GetThreadContext");
		return 0;
	}
	CONTEXT newContext = context;
#ifdef X_86
	newContext.Eip = (DWORD)baseAddr;
#else 
	newContext.Rip = (DWORD64)baseAddr;
#endif

	if (!SetThreadContext(hThread, &newContext)) {
		l->error(L"SetThreadContext");
		return 0;
	}

	if (ResumeThread(hThread) == -1) {
		l->error(L"ResumeThread");
		return 0;
	}

	//launch monitor thread
	l->info(L"Starting monitoring...");
	HANDLE controlThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)monitoring, as, 0, NULL);

	int eventWait = WaitForSingleObject(controlThread, 500000);
	if (eventWait == WAIT_TIMEOUT) {
		l->error(L"Timeout reached, DLL probably not injected :/");
		return 0;
	}

	//suspend thread and modify EIP
	if (SuspendThread(hThread) == -1) {
		l->error(L"SuspendThread");
		return 0;
	}

	//set old context to continue normal execution flow
	if (!SetThreadContext(hThread, &context)) {
		l->error(L"SetThreadContext");
		return 0;
	}

	if (ResumeThread(hThread) == -1) {
		l->error(L"ResumeThread");
		return 0;
	}

	CloseHandle(hThread);
	return 1;
}

int Injector::inject_NtQueueApcThread(LPVOID baseAddr) {
	l->error(L"Not yet implemented, wait a minute ;)");
	return 0;
}